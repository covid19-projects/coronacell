[CoronaCell](https://ezequielpuerta.gitlab.io/coronacell/) - Un simulador CellDEVS sobre el CoVid19 (Coronavirus)
=====================================

Se espera que este repositorio se clone en un directorio ubicado al mismo nivel que la carpeta de la [distribución original de CD++](https://github.com/SimulationEverywhere/CDPP_ExtendedStates-codename-Santi).

Es decir si dicha distribución (que al descargarse se ubica en CDPP_ExtendedStates-codename-Santi) está ubicada en una carpeta llamada "foo", clonar este repo (CoronaCell) en:

foo
* CDPP_ExtendedStates-codename-Santi
* CoronaCell