import getopt, sys, os, re

scenarioVariableName = "scenario"
symptomsAfterVariableName = "symptomsAfter"
recoveryAfterVariableName = "recoveryAfter"

originalLogFilename = r'results/log01.log'
populationLogFilename = r'results/population.js'

argumentList = sys.argv[1:]
shortOptions = "s:i:r:"
longOptions = [
    (("%s=") % scenarioVariableName),
    (("%s=") % symptomsAfterVariableName),
    (("%s=") % recoveryAfterVariableName)]

try:
    arguments, values = getopt.getopt(argumentList, shortOptions, longOptions)
except getopt.error as err:
    print (str(err))
    sys.exit(2)

symptomsAfterValue = 0
recoveryAfterValue = 0

for currentArgument, currentValue in arguments:
    if currentArgument in ("-s", (("--%s") % scenarioVariableName)):
        scenarioPathValue = currentValue
    if currentArgument in ("-i", (("--%s") % symptomsAfterVariableName)):
        symptomsAfterValue = int(currentValue)
        alreadyWithSymptoms = symptomsAfterValue + 1
    if currentArgument in ("-r", (("--%s") % recoveryAfterVariableName)):
        recoveryAfterValue = int(currentValue)

alreadyRecovered = symptomsAfterValue + recoveryAfterValue + 1

originalLogFilePath = os.path.join(scenarioPathValue, originalLogFilename)
populationLogFilePath = os.path.join(scenarioPathValue, populationLogFilename)

days = []
susceptibles = []
incubation = []
infected = []
recovered = []
dead = []
currentStatesByCell = {}

currentDay = 0
currentSusceptibles = 0
currentIncubation = 0
currentInfected = 0
currentRecovered = 0
currentDead = 0

################################################################################
regex_pattern = "^\d / . / . / \d{2}:\d{2}:\d{2}:(\d{3}):\d{1} / population\(\d*,\d*\)\((\d*)\) / out / *(-?\d*).*$"
fileIn = open(originalLogFilePath, "rt")

for line in fileIn:
    match = re.search(regex_pattern, line)

    if match:
        parsed_day = int(match.group(1))
        parsed_cell = int(match.group(2))
        parsed_state = int(match.group(3))

        if currentDay < parsed_day:
            days.append(currentDay)
            susceptibles.append(currentSusceptibles)
            incubation.append(currentIncubation)
            infected.append(currentInfected)
            recovered.append(currentRecovered)
            dead.append(currentDead)

            currentDay = parsed_day

        if parsed_cell in currentStatesByCell:
            previousState = currentStatesByCell[parsed_cell]

            if (parsed_state == 0) and (previousState != 0):
                currentSusceptibles = currentSusceptibles + 1

            elif (parsed_state == 1) and (previousState != 1):
                currentIncubation = currentIncubation + 1
                if previousState == 0:
                    currentSusceptibles = currentSusceptibles - 1

            elif (parsed_state == alreadyWithSymptoms) and (previousState != alreadyWithSymptoms):
                currentInfected = currentInfected + 1
                if (previousState > 0) and (previousState < alreadyWithSymptoms):
                    currentIncubation = currentIncubation - 1

            elif (parsed_state == alreadyRecovered) and (previousState != alreadyRecovered):
                currentRecovered = currentRecovered + 1
                if (previousState > 0) and (previousState < alreadyWithSymptoms):
                    currentIncubation = currentIncubation - 1
                elif (previousState >= alreadyWithSymptoms) and (previousState < alreadyRecovered):
                    currentInfected = currentInfected - 1

            elif (parsed_state == -1) and (previousState != -1):
                currentDead = currentDead + 1
                if (previousState > 0) and (previousState < alreadyWithSymptoms):
                    currentIncubation = currentIncubation - 1
                elif (previousState >= alreadyWithSymptoms) and (previousState < alreadyRecovered):
                    currentInfected = currentInfected - 1
        else:
            if parsed_state == 0:
                currentSusceptibles = currentSusceptibles + 1
            elif parsed_state == 1:
                currentIncubation = currentIncubation + 1

        currentStatesByCell[parsed_cell] = parsed_state

days.append(currentDay)
susceptibles.append(currentSusceptibles)
incubation.append(currentIncubation)
infected.append(currentInfected)
recovered.append(currentRecovered)
dead.append(currentDead)

fileIn.close()
################################################################################

fileOut = open(populationLogFilePath, "wt")

allCollections = {
    "days" : days,
    "susceptibles" : susceptibles,
    "incubation" : incubation,
    "infected" : infected,
    "recovered" : recovered,
    "dead" : dead}

for name, collection in allCollections.items():
    fileOut.write("var %s = [" % name)
    first = True
    for element in collection:
        if first:
            first = False
            fileOut.write(str(element))
        else:
            fileOut.write(",")
            fileOut.write(str(element))
    fileOut.write("];\n")

fileOut.close()
