#BeginMacro(susceptible)
  0
#EndMacro

#BeginMacro(incubation)
  1
#EndMacro

#BeginMacro(infected)
  alreadyWithSymptoms
#EndMacro

#BeginMacro(recovered)
  alreadyRecovered
#EndMacro

#BeginMacro(dead)
  -1
#EndMacro

#BeginMacro(imSusceptible)
  ((0,0)=0)
#EndMacro

#BeginMacro(imIncubating)
  (((0,0) != 0) and ((0,0) <= symptomsAfter))
#EndMacro

#BeginMacro(imInfected)
  ((0,0)>0 and (0,0)<=infectedUntil)
#EndMacro

#BeginMacro(imRecovered)
  (((0,0) > recoveryAfter) and not ((0,0)>0 and (0,0)<=infectedUntil))
#EndMacro

#BeginMacro(anyNeighborIsInfected)
  selectedNeighbors anySatisfy(any > 0 and any <= infectedUntil)
#EndMacro

#BeginMacro(gotInfected)
  (uniform(0,1) <= transmissionRate)
#EndMacro

#BeginMacro(died)
  (uniform(0,1) <= deathRate)
#EndMacro
