#!/bin/bash

mkdir -p ./results/
mkdir -p ./results/log/
cp ../macros.inc ./results/macros.inc.template

python ../scenario_generator.py -w 19 -h 19 -s $(pwd) -z "(9,9)" -i 7 -r 8 -x 1 -d 0 -t 1 -e 4

../../bin/cd++ -m ./results/scenario.ma -l ./results/log/log -t 00:00:00:100

rm ./results/macros.inc.template
rm ./results/macros.inc

mv ./results/log/log01 ./results/log01.log
rm -r ./results/log/

python ../population_log.py -s $(pwd) -i 7 -r 8
