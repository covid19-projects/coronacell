#!/bin/bash

mkdir -p ./results/
mkdir -p ./results/log/
cp ../macros.inc ./results/macros.inc.template

python ../scenario_generator.py -w 90 -h 90 -s $(pwd) -z "(20,20);(80,80)" -i 7 -r 8 -x 0.4 -d 0.005 -t 1 -e 4

../../bin/cd++ -m ./results/scenario.ma -l ./results/log/log -t 00:00:05:000

rm ./results/macros.inc.template
rm ./results/macros.inc

mv ./results/log/log01 ./results/log01.log
rm -r ./results/log/

python ../population_log.py -s $(pwd) -i 7 -r 8
