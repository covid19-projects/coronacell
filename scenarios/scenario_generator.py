# importo librerias necesarias
import getopt, sys, os, re

################################################################################
# definiciones para el resto del archivo y los templates
################################################################################

# defino los nombres de las variables del template
# (estas variables se podran usar en los templates)
# notar que las tasas son floats entre 0 y 1
widthVariableName = "width"
heightVariableName = "height"
scenarioVariableName = "scenario"
patientZeroVariableName = "patientZero"
symptomsAfterVariableName = "symptomsAfter"
alreadyWithSymptomsVariableName = "alreadyWithSymptoms"
infectedUntilVariableName = "infectedUntil"
recoveryAfterVariableName = "recoveryAfter"
alreadyRecoveredVariableName = "alreadyRecovered"
transmissionRateVariableName = "transmissionRate"
deathRateVariableName = "deathRate"
travelRadiusVariableName = "travelRadius"
numberOfEncountersVariableName = "numberOfEncounters"
selectedNeighborsVariableName = "selectedNeighbors"
childrenPercentageVariableName = "childrenPercentage"
olderAdultsPercentageVariableName = "olderAdultsPercentage"

# defino los nombres de archivos genericos a contemplar
scenarioTemplateFilename = r'scenario.template'
scenarioModelFilename = r'results/scenario.ma'
scenarioValuesFilename = r'results/scenario.val'
macrosTemplateFilename = r'results/macros.inc.template'
macrosFilename = r'results/macros.inc'
paletteFilename = r'results/palette.pal'

################################################################################
# argumentos desde la terminal
################################################################################

# obtengo los parametros por consola y creo las listas de parametros esperados
argumentList = sys.argv[1:]
shortOptions = "w:h:s:z:r:i:x:d:t:e:c:o:"
longOptions = [
    (("%s=") % widthVariableName),
    (("%s=") % heightVariableName),
    (("%s=") % scenarioVariableName),
    (("%s=") % patientZeroVariableName),
    (("%s=") % recoveryAfterVariableName),
    (("%s=") % symptomsAfterVariableName),
    (("%s=") % transmissionRateVariableName),
    (("%s=") % deathRateVariableName),
    (("%s=") % travelRadiusVariableName),
    (("%s=") % numberOfEncountersVariableName),
    (("%s=") % childrenPercentageVariableName),
    (("%s=") % olderAdultsPercentageVariableName)]

# parseo los argumentos
try:
    arguments, values = getopt.getopt(argumentList, shortOptions, longOptions)
except getopt.error as err:
    print (str(err))
    sys.exit(2)

# y obtengo los valores de cada uno de ellos
for currentArgument, currentValue in arguments:
    if currentArgument in ("-w", (("--%s") % widthVariableName)):
        widthValue = currentValue
    if currentArgument in ("-h", (("--%s") % heightVariableName)):
        heightValue = currentValue
    if currentArgument in ("-s", (("--%s") % scenarioVariableName)):
        scenarioPathValue = currentValue
    if currentArgument in ("-z", (("--%s") % patientZeroVariableName)):
        patientZeroValue = currentValue
    if currentArgument in ("-r", (("--%s") % recoveryAfterVariableName)):
        recoveryAfterValue = currentValue
    if currentArgument in ("-i", (("--%s") % symptomsAfterVariableName)):
        symptomsAfterValue = currentValue
    if currentArgument in ("-x", (("--%s") % transmissionRateVariableName)):
        transmissionRateValue = currentValue
    if currentArgument in ("-d", (("--%s") % deathRateVariableName)):
        deathRateValue = currentValue
    if currentArgument in ("-t", (("--%s") % travelRadiusVariableName)):
        travelRadiusValue = currentValue
    if currentArgument in ("-e", (("--%s") % numberOfEncountersVariableName)):
        numberOfEncountersValue = currentValue
    if currentArgument in ("-c", (("--%s") % childrenPercentageVariableName)):
        childrenPercentageValue = int(currentValue)
    if currentArgument in ("-o", (("--%s") % olderAdultsPercentageVariableName)):
        olderAdultsPercentageValue = int(currentValue)

################################################################################
# diccionarios para reemplazar variables en los templates
################################################################################

# separo los argumentos para el archivo de modelo y para el de valores
# algunos valores son los provistos, otros se deducen en base a los obtenidos
modelArguments = {
    widthVariableName : widthValue,
    heightVariableName : heightValue,
    symptomsAfterVariableName : symptomsAfterValue,
    alreadyWithSymptomsVariableName : str(int(symptomsAfterValue)+1),
    recoveryAfterVariableName : recoveryAfterValue,
    alreadyRecoveredVariableName : str(int(recoveryAfterValue)+1),
    transmissionRateVariableName : transmissionRateValue,
    deathRateVariableName : deathRateValue}

if int(symptomsAfterValue)+int(recoveryAfterValue) == 0:
    modelArguments[infectedUntilVariableName] = "1"
else:
    diseaseDuration = str(int(symptomsAfterValue)+int(recoveryAfterValue))
    modelArguments[infectedUntilVariableName] = diseaseDuration

valuesArguments = {
    patientZeroVariableName : patientZeroValue}

# segun el directorio del escenario que se quiere simular, construyo los paths
# completos de los archivos que estamos contemplando
scenarioTemplateFilePath = os.path.join(scenarioPathValue, scenarioTemplateFilename)
scenarioModelFilePath = os.path.join(scenarioPathValue, scenarioModelFilename)
scenarioValuesFilePath = os.path.join(scenarioPathValue, scenarioValuesFilename)
macrosTemplateFilePath = os.path.join(scenarioPathValue, macrosTemplateFilename)
macrosFilePath = os.path.join(scenarioPathValue, macrosFilename)
paletteFilePath = os.path.join(scenarioPathValue, paletteFilename)

################################################################################
# creo los vecinos en base a los viajes y los encuentros
################################################################################

filteredNeighbors = {"(0,0,0)", "(0,0,1)"}
top_neighbors = {"(-1,0,0)"}
right_neighbors = {"(0,1,0)"}
bottom_neighbors = {"(1,0,0)"}
left_neighbors = {"(0,-1,0)"}
all_neighbors = {
    0 : top_neighbors,
    1 : right_neighbors,
    2 : bottom_neighbors,
    3 : left_neighbors}

for radius in range(1, int(travelRadiusValue)+1):
    negative = radius*(-1)
    positive = radius
    for distance in range(negative, positive+1):
        top_neighbors.add("(%d,%d,0)" % (negative, distance))
        right_neighbors.add("(%d,%d,0)" % (distance, positive))
        bottom_neighbors.add("(%d,%d,0)" % (positive, distance))
        left_neighbors.add("(%d,%d,0)" % (distance, negative))

for i in range(1, int(numberOfEncountersValue)+1):
    filteredNeighbors.add(all_neighbors[i % 4].pop())

modelArguments[selectedNeighborsVariableName] = " ".join(filteredNeighbors)
filteredNeighbors.remove("(0,0,1)")
selectedRealNeighbors = filteredNeighbors
################################################################################
# creo el archivo del modelo
################################################################################

# abro el template correspondiente
fileIn = open(scenarioTemplateFilePath, "rt")
# y creo el archivo para el modelo de escenario requerido
fileOut = open(scenarioModelFilePath, "wt")

# reemplazo las variables del template con los argumentos obtenidos
# (menos el del path del escenario seleccionado)
for line in fileIn:
    resultingLine = line
    for currentArgument, currentValue in modelArguments.items():
        resultingLine = resultingLine.replace(currentArgument, currentValue)
    fileOut.write(resultingLine)

fileIn.close()
fileOut.close()

################################################################################
# creo el archivo de macros (notar que puede haber bucle condicional para los vecinos)
################################################################################

# patron para encontrar bucle OR (primer grupo: conjunto, segundo grupo: condicion)
or_pattern = "^ *(\w*) anySatisfy\((.*)\)$"
# abro el template correspondiente
fileIn = open(macrosTemplateFilePath, "rt")
# y creo el archivo para el modelo de escenario requerido
fileOut = open(macrosFilePath, "wt")

# reemplazo las variables del template con los argumentos obtenidos
for line in fileIn:
    resultingLine = line

    match = re.search(or_pattern, resultingLine)
    if match:
        parsedAny = set()
        for each in selectedRealNeighbors:
            parsedAny.add("(%s)" % (match.group(2).replace("any", each)))

        resultingLine = parsedAny.pop()
        for each in parsedAny:
            resultingLine = "(%s or %s)" % (each, resultingLine)

        resultingLine = "\t" + resultingLine + "\n"

    for currentArgument, currentValue in modelArguments.items():
        resultingLine = resultingLine.replace(currentArgument, currentValue)
    fileOut.write(resultingLine)

fileIn.close()
fileOut.close()

################################################################################
# creo la paleta de colores
################################################################################

# creo el archivo para la paleta
fileOut = open(paletteFilePath, "wt")

# color para los susceptibles
fileOut.write("[0.0;0.9] 238 238 238\n")
# color para los que estan incubando (si es que hay periodo de incubacion)
if symptomsAfterValue != "0":
    fileOut.write(("[1.0;%s.9] 255 150 150\n") % symptomsAfterValue)
    # si hay periodo de recuperacion...
    if recoveryAfterValue != "0":
        # agrego colores para los infectados con sintomas y el periodo de recuperacion
        endOfDisease = str(int(diseaseDuration)+1)
        fileOut.write(("[%s.0;%s.9] 255 68 68\n") % (modelArguments[alreadyWithSymptomsVariableName], diseaseDuration))
        fileOut.write(("[%s.0;%s.9] 150 200 150\n") % (endOfDisease, endOfDisease))
    else:
        # sino, solo para los infectados con sintomas ya que nunca se recuperan
        fileOut.write(("[%s.0;%s.9] 255 68 68\n") % (modelArguments[alreadyWithSymptomsVariableName], modelArguments[alreadyWithSymptomsVariableName]))
# si no hay periodo de incubacion...
else:
    # y hay periodo de recuperacion...
    if recoveryAfterValue != "0":
        # agrego colores para los infectados con sintomas y el periodo de recuperacion
        fileOut.write(("[1.0;%s.9] 255 68 68\n") % recoveryAfterValue)
        fileOut.write(("[%s.0;%s.9] 150 200 150\n") % (modelArguments[alreadyRecoveredVariableName], modelArguments[alreadyRecoveredVariableName]))
    else:
        # sino, solo para los infectados con sintomas ya que nunca se recuperan
        fileOut.write("[1.0;1.9] 255 68 68\n")

# si hay tasa de mortalidad, agrego el color para ese evento
if float(deathRateValue) != 0.0:
    fileOut.write("[-1.0;-0.1] 80 80 80\n")

fileOut.close()

################################################################################
# creo el archivo para los valores iniciales
################################################################################
fileOut = open(scenarioValuesFilePath, "wt")

# si los porcentajes superan el 100 por ciento, pongo valores por default
if childrenPercentageValue + olderAdultsPercentageValue > 100:
    childrenPercentageValue = 19
    olderAdultsPercentage = 16

# los chicos seran 0, los ancianos 2. Los adultos jovenes seran 1
population = int(widthValue) * int(heightValue)
childrensAmount = int(childrenPercentageValue * population / 100)
olderAdultsAmount = int(olderAdultsPercentageValue * population / 100)
restAmount = population - childrensAmount - olderAdultsAmount
allPopulation = set()

print "empieza el bucle"
for i in range(childrensAmount):
    allPopulation.add("0")
    print allPopulation
for i in range(olderAdultsAmount):
    allPopulation.add("2")
for i in range(restAmount):
    allPopulation.add("1")

allPatients = patientZeroValue.split(';')

for row in range(int(widthValue)):
    for column in range(int(heightValue)):
        current = "(%s,%s)" % (row, column)
        if current in allPatients:
            fileOut.write(("(%s,%s,0) = 1\n") % (row, column))
        fileOut.write(("(%s,%s,1) = %s\n") % (row, column, allPopulation.pop()))

fileOut.close()
