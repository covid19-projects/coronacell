var ctx = document.getElementById("populationChart");
var stackedLine = new Chart(ctx, {
    type: 'line',
    options: {
        legend: {
            display: true,
            position: 'bottom'
        },
        elements: {
            point: {
                radius: 0
            }
        },
        responsive: true,
        tooltips: {
            mode: 'index',
        },
        hover: {
            mode: 'index'
        },
        scales: {
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Día'
                }
            }],
            yAxes: [{
                stacked: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Personas'
                }
            }]
        }
    },
    data: {
        labels: days,
        datasets: [
            {
                data: incubation,
                label: "Incubación - Asintomáticos",
                borderColor: "#FF9696",
                fill: true,
                backgroundColor: "#FF9696"
            },
            {
                data: infected,
                label: "Infectados - Sintomáticos",
                borderColor: "#FF4444",
                fill: true,
                backgroundColor: "#FF4444"
            },
            {
                data: dead,
                label: "Fallecidos",
                borderColor: "#505050",
                fill: true,
                backgroundColor: "#505050"
            },
            {
                data: recovered,
                label: "Recuperados",
                borderColor: "#96C896",
                fill: true,
                backgroundColor: "#96C896"
            },
            {
                data: susceptibles,
                label: "Susceptibles",
                borderColor: "#EEEEEE",
                fill: true,
                backgroundColor: "#EEEEEE"
            }
        ]
    }
});
