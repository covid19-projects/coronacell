import http.server
import socketserver
import getopt, sys

argumentList = sys.argv[1:]
shortOptions = "p:"
longOptions = ["port="]

try:
    arguments, values = getopt.getopt(argumentList, shortOptions, longOptions)
except getopt.error as err:
    print (str(err))
    sys.exit(2)

for currentArgument, currentValue in arguments:
    if currentArgument in ("-p", "--port"):
        port = int(currentValue)
    else:
        port = 8080

Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", port), Handler) as httpd:
    print("serving at port", port)
    httpd.serve_forever()
